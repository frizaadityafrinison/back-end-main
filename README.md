# Test-Driven Development (Chapter 8 - Main Challenge)

Heroku Website: https://friza-challenge08.herokuapp.com/

## Tech Stack
  * Node.js 16.14 (Runtime Environment)
  * Express 4.17.3 (HTTP Server)
  * Nodemon 2.0.16 (Server Runner)
  * Jest 28.1.1 (Testing Library)

## Prerequisite

These are things that you need to install on your machine before proceed to installation
* [Node.js](https://nodejs.org/)
* [NPM (Package Manager)](https://www.npmjs.com/)
* [PostgreSQL](https://www.postgresql.org/) 
* [Postman](https://www.postman.com/)


## Installation

Clone this repository

```bash
cd desired_directory/
git clone https://gitlab.com/frizaadityafrinison/back-end-main
cd back-end-main
```

Download all the package and it's dependencies
```bash
npm install 
```

Install the Sequelize
```bash
npm install sequelize-cli -g
```

Config the Database
```bash
cd app/
cat config/config.json
```

Create The Database
```bash
sequelize db:create
```

Run The Migration
```bash
sequelize db:migrate
``` 

Seed The Data
```bash
sequelize db:seed:all
``` 


## Run The Server

Run the following command to start the server

```bash
  npm run start
```

Open [http://localhost:8000](http://localhost:8000) to view it in your browser.

## Test The Server

Run the following command to start test the server using Jest 
```bash
  npm test
```

## Screenshot
![_BD1D93C4-5D39-43EE-9ADA-633542E498B8_](/uploads/75ec7805cffcaccbab0e94bf827c33b4/_BD1D93C4-5D39-43EE-9ADA-633542E498B8_.png)
![_C3074662-1BFA-467D-BC1B-C04C4E43A3A3_](/uploads/bae50a42299eba780b348c06ac9efa38/_C3074662-1BFA-467D-BC1B-C04C4E43A3A3_.png)
