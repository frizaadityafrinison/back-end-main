/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-undef */
const ApplicationController = require('../ApplicationController')
const { NotFoundError } = require('../../errors')

describe('ApplicationController', () => {
  const applicationController = new ApplicationController()

  test('#handleGetRoot', () => { // -----------------------------------HANDLE GET ROOT
    const mockJSON = {
      status: 'OK',
      message: "BCR API is up and running!",
    }
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn().mockReturnThis()
    }
    const mockReq = { }

    applicationController.handleGetRoot(mockReq, mockRes)

    expect(mockRes.status).toHaveBeenCalledWith(200)
    expect(mockRes.json).toHaveBeenCalledWith(mockJSON)
  })

  test('#handleNotFound', () => { // -----------------------------------HANDLE NOT FOUND
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn().mockReturnThis()
    }
    const mockReq = {
      method: 'Er RAWR!',
      url: 'http://localhost:8080/errawr'
    }
    const err = new NotFoundError(mockReq.method, mockReq.url)
  
    applicationController.handleNotFound(mockReq, mockRes)

    expect(mockRes.status).toHaveBeenCalledWith(404)
    expect(mockRes.json).toHaveBeenCalledWith({
      error: {
        name: err.name,
        message: err.message,
        details: err.details
      }
    })
  })

  test('#handleError', () => { // -----------------------------------HANDLE ERROR
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn().mockReturnThis()
    }
    const mockReq = { }
    const err = new Error('Er RAWR!')
    
    applicationController.handleError(err, mockReq, mockRes)

    expect(mockRes.status).toHaveBeenCalledWith(500)
    expect(mockRes.json).toHaveBeenCalledWith({
      error: {
        name: err.name,
        message: err.message,
        details: err.details || null
      }
    })
  })

  test('#getOffsetFromRequest', () => { // -----------------------------------GET OFFSET FROM REQUEST
    const mockReq = {
      query: {
        page: 1,
        pageSize: 10
      }
    }

    const offset = applicationController.getOffsetFromRequest(mockReq)
    expect(offset).toEqual(0)
  })

  test('#buildPaginationObject', () => { // -----------------------------------BUILD PAGINATION OBJECT
    const mockReq = {
      query: {
        page: 1,
        pageSize: 10
      }
    }

    const mockCount = 10
    const returnValue = applicationController.buildPaginationObject(mockReq, mockCount)

    expect(returnValue).toEqual({
      page: 1,
      pageCount: 1,
      pageSize: 10,
      count: mockCount
    })
  })
})