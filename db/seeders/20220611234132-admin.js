"use strict";
const bcrypt = require("bcryptjs");
const { Role } = require("../../app/models");

module.exports = {
  async up(queryInterface, Sequelize) {
    const password = "123";
    const encryptedPassword = bcrypt.hashSync(password, 10);

    const role = await Role.findOne({
      where: {
        name: "ADMIN",
      },
    });

    await queryInterface.bulkInsert(
      "Users",
      [
        {
          name: "Friza Aditya",
          email: "frizaaditya09@onionmail.com",
          encryptedPassword,
          roleId: role.id,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {

  },
};
